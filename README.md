# NMEA Toolkit (golang)

A programming toolkit for interacting with NMEA data streams. Written in Go
(golang).

[![GoDoc](https://godoc.org/bitbucket.org/mabgo/nmea?status.svg)](https://godoc.org/bitbucket.org/mabgo/nmea)
